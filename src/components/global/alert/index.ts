import { defineComponent, PropType, h, computed, resolveComponent } from 'vue'
import { AlertVariant } from '~/typings/alert'
import { GLOBAL_COMPONENT_PREFIX as PREFIX } from '~/utils/configs'
import IconError from '~/assets/images/icons/error.svg?component'
import IconCheck from '~/assets/images/icons/check.svg?component'

import './index.css'

export const COMPONENT_NAME = `${PREFIX}alert`

export default defineComponent({
  name: COMPONENT_NAME,

  components: {
    IconError,
    IconCheck,
  },

  props: {
    tag: {
      type: String,
      default: 'div',
    },
    variant: {
      type: String as PropType<AlertVariant>,
      default: 'secondary',
    },
  },

  setup(props) {
    const classes = computed(() => [
      `${COMPONENT_NAME}`,
      `${COMPONENT_NAME}--${props.variant}`,
    ])

    return {
      classes,
    }
  },
  render() {
    return h(
      this.tag,
      {
        class: this.classes,
      },
      [
        this.variant === 'error' || this.variant === 'success'
          ? h(
              'span',
              {
                class: `${COMPONENT_NAME}__icon mr-3`,
              },
              [
                this.variant === 'error'
                  ? h(resolveComponent('IconError'), { class: 'icon-stroke' })
                  : this.variant === 'success'
                  ? h(resolveComponent('IconCheck'), { class: 'icon-stroke' })
                  : null,
              ]
            )
          : null,
        this.$slots.default && this.$slots.default(),
      ]
    )
  },
})
