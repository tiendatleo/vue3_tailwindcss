import { defineComponent, resolveComponent, PropType, h, computed } from 'vue'
import { Size } from '~/typings/components'
import { ButtonVariant, ButtonShape } from '~/typings/button'
import { GLOBAL_COMPONENT_PREFIX as PREFIX } from '~/utils/configs'

import './index.css'

export const COMPONENT_NAME = `${PREFIX}button`

export default defineComponent({
  name: COMPONENT_NAME,

  props: {
    tag: {
      type: String,
      default: 'button',
    },
    size: {
      type: String as PropType<Size>,
      default: 'md',
    },
    xl: {
      // Size of component on 'xl' breakpoint
      type: String as PropType<Size>,
      default: '',
    },
    variant: {
      type: String as PropType<ButtonVariant>,
      default: 'blue',
    },
    shape: {
      type: String as PropType<ButtonShape>,
      default: 'rounded',
    },
    loading: Boolean,
    to: {
      type: [String, Object], // Vue-router prop. Denotes the target route of the link.
      default: '',
    },
  },

  setup(props) {
    const classes = computed(() => [
      `${PREFIX}button`,
      `${PREFIX}button--${props.size}`,
      `${PREFIX}button--${props.variant}`,
      props.xl && `${PREFIX}button--xl-${props.xl}`,
      props.shape !== 'rounded' && `${PREFIX}button--${props.shape}`,
      props.loading && 'pointer-events-none',
    ])

    return {
      classes,
    }
  },
  render() {
    const Spinner = resolveComponent('v-spinner')

    return h(
      this.to ? resolveComponent('router-link') : this.tag,
      {
        class: this.classes,
        to: this.to,
      },
      [
        this.loading
          ? h(Spinner, {
              class: 'mr-2',
              variant: this.variant.includes('outline') ? 'primary' : 'white',
              size: 'sm',
            })
          : null,
        this.$slots.default && this.$slots.default(),
      ]
    )
  },
})
