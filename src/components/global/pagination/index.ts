import { defineComponent, PropType, h, computed, resolveComponent } from 'vue'
import { Size } from '~/typings/components'
import { GLOBAL_COMPONENT_PREFIX as PREFIX } from '~/utils/configs'
import IconCaretRight from '~/assets/images/icons/caret-right.svg?component'

import './index.css'

export const COMPONENT_NAME = `${PREFIX}pagination`

export default defineComponent({
  name: COMPONENT_NAME,

  components: {
    IconCaretRight,
  },

  props: {
    tag: {
      type: String,
      default: 'nav',
    },
    size: {
      type: String as PropType<Size>,
      default: 'md',
    },
    modelValue: {
      // current page
      type: Number,
      required: true,
      default: 1,
    },
    pageSize: {
      type: Number,
      required: true,
    },
    total: {
      type: Number,
      required: true,
    },
    totalText: {
      type: Function as PropType<
        (modelValue: number, total: number, pageSize: number) => string
      >,
      default: () => '',
    },
    hideOnSinglePage: Boolean,
    maxVisibleButtons: {
      type: Number,
      default: 5,
    },
    prevText: {
      type: String,
      default: '',
    },
    nextText: {
      type: String,
      default: '',
    },
    listWrapperClass: {
      type: [Object, Array, String],
      default: () => ({}),
    },
  },

  emits: ['update:modelValue'],

  setup(props, { slots, emit }) {
    const totalPages = computed(() => {
      return props.pageSize ? Math.ceil(props.total / props.pageSize) : 0
    })

    // https://css-tricks.com/creating-a-reusable-pagination-component-in-vue/
    const paginationTriggers = computed(() => {
      if (!totalPages.value) return []

      const { modelValue, maxVisibleButtons } = props
      const visiblePagesThreshold = Math.floor((maxVisibleButtons - 1) / 2)
      const pagintationTriggersArray = Array(
        (totalPages.value < maxVisibleButtons
          ? totalPages.value
          : maxVisibleButtons) - 1
      ).fill(0)

      if (modelValue <= visiblePagesThreshold + 1) {
        pagintationTriggersArray[0] = 1
        const pagintationTriggers = pagintationTriggersArray.map(
          (paginationTrigger, index) => {
            return pagintationTriggersArray[0] + index
          }
        )

        if (totalPages.value > 1 && totalPages.value < maxVisibleButtons) {
          pagintationTriggers.push(totalPages.value)
        } else if (totalPages.value >= maxVisibleButtons) {
          pagintationTriggers.push(0, totalPages.value)
        }

        return pagintationTriggers
      }

      if (modelValue >= totalPages.value - visiblePagesThreshold) {
        const pagintationTriggers = pagintationTriggersArray.map(
          (paginationTrigger, index) => {
            return totalPages.value - index
          }
        )
        pagintationTriggers.reverse().unshift(1, -1)
        return pagintationTriggers
      }

      pagintationTriggersArray[0] = modelValue - visiblePagesThreshold + 1
      const pagintationTriggers = pagintationTriggersArray.map(
        (paginationTrigger, index) => {
          return pagintationTriggersArray[0] + index - 1
        }
      )
      pagintationTriggers.unshift(1, -1)
      pagintationTriggers.pop()
      pagintationTriggers.push(0, totalPages.value)
      return pagintationTriggers
    })

    const handlePrev = (e: Event) => {
      e.preventDefault()
      emit('update:modelValue', props.modelValue - 1)
    }

    const handleNext = (e: Event) => {
      e.preventDefault()
      emit('update:modelValue', props.modelValue + 1)
    }

    const handleChange = (page: number) => {
      if (page > 0) emit('update:modelValue', page)
    }

    const makePrevButton = () => {
      const { prevText } = props
      const disabled = props.modelValue <= 1

      return slots.prevButton
        ? slots.prevButton({ disabled, prevText, handlePrev })
        : h(
            'li',
            {
              class: [`${COMPONENT_NAME}__item`],
            },
            [
              h(
                'button',
                {
                  disabled,
                  title: prevText,
                  class: [
                    `${COMPONENT_NAME}__item-link`,
                    disabled ? 'opacity-50 pointer-events-none' : '',
                  ],
                  onClick: handlePrev,
                },
                prevText ||
                  h(resolveComponent('IconCaretRight'), { class: 'transform rotate-180' })
              ),
            ]
          )
    }

    const makeNextButton = () => {
      const { nextText, modelValue } = props
      const disabled = modelValue >= totalPages.value

      return slots.prevButton
        ? slots.prevButton({ disabled, nextText, handleNext })
        : h(
            'li',
            {
              class: [`${COMPONENT_NAME}__item`],
            },
            [
              h(
                'button',
                {
                  disabled,
                  title: nextText,
                  class: [
                    `${COMPONENT_NAME}__item-link`,
                    disabled ? 'opacity-50 pointer-events-none' : '',
                  ],
                  onClick: handleNext,
                },
                [nextText || h(IconCaretRight)]
              ),
            ]
          )
    }

    return {
      totalPages,
      paginationTriggers,
      makePrevButton,
      makeNextButton,
      handleChange,
    }
  },

  render() {
    const prevButton = this.totalPages ? this.makePrevButton() : null
    const nextButton = this.totalPages ? this.makeNextButton() : null

    if (this.hideOnSinglePage && this.totalPages <= 1) return null
    return h(
      this.tag,
      {
        class: [COMPONENT_NAME, `${COMPONENT_NAME}--${this.size}`],
      },
      [
        this.totalText || this.$slots.totalText
          ? h(
              'span',
              {
                class: `${COMPONENT_NAME}__total-text`,
              },
              this.$slots.totalText
                ? this.$slots.totalText({
                    modelValue: this.modelValue,
                    total: this.total,
                    pageSize: this.pageSize,
                  })
                : this.totalText(this.modelValue, this.total, this.pageSize)
            )
          : null,

        h(
          'ul',
          {
            class: [`${COMPONENT_NAME}__ul`, this.listWrapperClass],
          },
          [
            prevButton,
            ...this.paginationTriggers.map((page) => {
              return h(
                'li',
                {
                  key: page,
                  class: [
                    `${COMPONENT_NAME}__item`,
                    {
                      [`${COMPONENT_NAME}__item--current`]:
                        page === this.modelValue,
                      [`${COMPONENT_NAME}__item--dumb`]: page <= 0,
                    },
                  ],
                },
                [
                  this.$slots.itemRender
                    ? this.$slots.itemRender({
                        page,
                        text: page <= 0 ? '•••' : page,
                        isDumb: page <= 0,
                        linkClass: `${COMPONENT_NAME}__item-link`,
                        handleClick: this.handleChange,
                      })
                    : h(
                        'button',
                        {
                          title: '',
                          class: `${COMPONENT_NAME}__item-link`,
                          onClick: () => this.handleChange(page),
                        },
                        page <= 0 ? '•••' : page
                      ),
                ]
              )
            }),
            nextButton,
          ]
        ),
      ]
    )
  },
})
