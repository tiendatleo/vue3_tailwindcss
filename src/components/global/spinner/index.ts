import { defineComponent, PropType, h, computed } from 'vue'
import { Size } from '~/typings/components'
import { SpinnerVariant } from '~/typings/spinner'
import { GLOBAL_COMPONENT_PREFIX as PREFIX } from '~/utils/configs'

import './index.css'

export const COMPONENT_NAME = `${PREFIX}spinner`

export default defineComponent({
  name: COMPONENT_NAME,

  props: {
    tag: {
      type: String,
      default: 'div',
    },
    size: {
      type: String as PropType<Size>,
      default: 'lg',
    },
    variant: {
      type: String as PropType<SpinnerVariant>,
      default: 'primary',
    },
    text: {
      type: String,
      default: '',
    },
  },

  setup(props) {
    const classes = computed(() => [
      `${COMPONENT_NAME}`,
      `${COMPONENT_NAME}--${props.size}`,
      `${COMPONENT_NAME}--${props.variant}`,
    ])

    return {
      classes,
    }
  },

  render() {
    const circle = () => h('span', { class: `${COMPONENT_NAME}__spin` })
    const text = () =>
      this.$slots.default
        ? this.$slots.default()
        : this.text && h('span', { class: 'app-spin__text' }, this.text)

    return h(
      'div',
      {
        class: [COMPONENT_NAME, this.classes, this.$attrs.class],
      },
      [circle(), text()]
    )
  },
})
