import { defineComponent, PropType, h, computed, watch, ref } from 'vue'
import { Size } from '~/typings/components'
import { GLOBAL_COMPONENT_PREFIX as PREFIX } from '~/utils/configs'
import { printWarning } from '~/utils/handle-error'
import IconError from '~/assets/images/icons/error.svg?component'
import IconCheckCircle from '~/assets/images/icons/check-circle.svg?component'

import './index.css'

export const COMPONENT_NAME = `${PREFIX}textarea`

export default defineComponent({
  name: COMPONENT_NAME,

  components: {
    IconError,
    IconCheckCircle,
  },

  props: {
    modelValue: {
      type: String,
      default: '',
    },
    tag: {
      type: String,
      default: 'div',
    },
    size: {
      type: String as PropType<Size>,
      default: 'md',
    },
    xl: {
      // Size of component on 'xl' breakpoint
      type: String as PropType<Size>,
      default: '',
    },
    loading: Boolean,
    prepend: {
      type: String,
      default: undefined,
    },
    prependClass: {
      type: [String, Array, Object],
      default: undefined,
    },
    append: {
      type: String,
      default: undefined,
    },
    appendClass: {
      type: [String, Array, Object],
      default: undefined,
    },
    wrapperClass: {
      type: [String, Array, Object],
      default: undefined,
    },
    valid: {
      type: [Boolean, Object],
      default: null,
      validator: (value: boolean | null) => {
        if ([null, true, false].findIndex((item) => item === value) > -1) {
          return true
        } else {
          printWarning(
            `${COMPONENT_NAME} prop 'valid' controls the validation state appearance of the component. 'true' for valid, 'false' for invalid, or 'null' for no validation state.\nIt's must be in [null, true, false]. Current: ${value}`
          )
          return false
        }
      },
    },
    rows: {
      type: [Number, String],
      default: 3,
    },
  },

  emits: ['update:modelValue'],

  setup(props, { emit }) {
    const classes = computed(() => [
      `${PREFIX}textarea`,
      `${PREFIX}textarea--${props.size}`,
      props.xl && `${PREFIX}textarea--xl-${props.xl}`,
      props.valid === true && `${PREFIX}textarea--valid`,
      props.valid === false && `${PREFIX}textarea--invalid`,
    ])

    const localValue = ref(props.modelValue)

    watch(
      () => props.modelValue,
      (newValue) => {
        localValue.value = newValue
      }
    )

    watch(localValue, (newValue) => {
      emit('update:modelValue', newValue)
    })

    const handleInput = (event: Event) => {
      const target = event.target as HTMLInputElement
      localValue.value = target.value
    }

    return {
      classes,
      localValue,
      handleInput,
    }
  },

  render() {
    return h(
      this.tag,
      {
        class: [COMPONENT_NAME, this.classes, this.wrapperClass],
      },
      [
        // Prepend
        (this.$slots.prepend || this.prepend) &&
          h(
            'div',
            {
              class: [`${COMPONENT_NAME}__prepend `, this.prependClass],
            },
            [this.$slots.prepend ? this.$slots.prepend() : this.prepend]
          ),

        // Input
        h('textarea', {
          ...this.$attrs,
          class: `${COMPONENT_NAME}__input`,
          rows: this.rows,
          value: this.localValue,
          onInput: this.handleInput,
        }),

        // Append
        (this.$slots.append || this.append) &&
          h(
            'div',
            {
              class: [`${COMPONENT_NAME}__append `, this.appendClass],
            },
            [this.$slots.append ? this.$slots.append() : this.append]
          ),

        // Validation state
        this.valid != null &&
          h(
            'div',
            {
              class: [
                `${COMPONENT_NAME}__append ${COMPONENT_NAME}__append--valid`,
              ],
            },
            [
              h(this.valid ? IconCheckCircle : IconError, {
                class: this.valid
                  ? 'icon text-success'
                  : 'icon-stroke text-error',
              }),
            ]
          ),
      ]
    )
  },
})
