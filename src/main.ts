import { createApp } from 'vue'
import App from './App.vue'
import router from '~/router'
import globalComponents from './plugins/global-components'
import mobileDetect from './plugins/mobile-detect'
//@ts-ignore
import moshaToast from 'mosha-vue-toastify'
import dayjs from 'dayjs'
import localizedFormat from 'dayjs/plugin/localizedFormat'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import advancedFormat from 'dayjs/plugin/advancedFormat'
import duration from 'dayjs/plugin/duration'

import 'mosha-vue-toastify/dist/style.css'
import '~/index.css'

dayjs.extend(localizedFormat)
dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.extend(customParseFormat)
dayjs.extend(advancedFormat)
dayjs.extend(duration)

// dayjs.tz.setDefault('Australia/Sydney')

const app = createApp(App)

app
  .use(router)
  .use(globalComponents)
  .use(mobileDetect)
  .use(moshaToast)
  .mount('#app')
