/* eslint-disable vue/component-definition-name-casing */
import { App } from 'vue'
import Button from '~/components/global/button'
import Alert from '~/components/global/alert'
import Tabs from '~/components/global/tabs/index.vue'
import Link from '~/components/global/link/index.vue'
import Input from '~/components/global/input'
import Textarea from '~/components/global/textarea'
import Empty from '~/components/global/empty/index.vue'
import Modal from '~/components/global/modal/index.vue'
import Switch from '~/components/global/switch/index.vue'
import Radio from '~/components/global/radio/index.vue'
import Checkbox from '~/components/global/checkbox/index.vue'
import Pagination from '~/components/global/pagination'
import Spinner from '~/components/global/spinner'
import Skeleton from '~/components/global/skeleton/index.vue'

export default (app: App) => {
  app.component(Button.name, Button)
  app.component(Alert.name, Alert)
  app.component(Tabs.name, Tabs)
  app.component(Link.name, Link)
  app.component(Input.name, Input)
  app.component(Textarea.name, Textarea)
  app.component(Empty.name, Empty)
  app.component(Modal.name, Modal)
  app.component(Switch.name, Switch)
  app.component(Radio.name, Radio)
  app.component(Checkbox.name, Checkbox)
  app.component(Pagination.name, Pagination)
  app.component(Spinner.name, Spinner)
  app.component(Skeleton.name, Skeleton)
}
