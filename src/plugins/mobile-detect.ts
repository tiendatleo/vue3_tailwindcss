import { App } from 'vue'
import { detectMobile } from '~/utils/helpers'

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $isMobile: () => boolean
  }
}

export default {
  install: (app: App) => {
    app.config.globalProperties.$isMobile = detectMobile
  },
}
