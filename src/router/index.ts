import { RouteRecordRaw, createRouter, createWebHistory } from 'vue-router'

/* Component preview pages */
const TypographyPage = () => import('~/pages/components/typography.vue')
const ButtonPage = () => import('~/pages/components/button.vue')
const AlertPage = () => import('~/pages/components/alert.vue')
const CardsPage = () => import('~/pages/components/cards.vue')
const InputPage = () => import('~/pages/components/input.vue')
const EmptyPage = () => import('~/pages/components/empty.vue')
const ModalPage = () => import('~/pages/components/modal.vue')
const SwitchPage = () => import('~/pages/components/switch.vue')
const RadioPage = () => import('~/pages/components/radio.vue')
const CheckboxPage = () => import('~/pages/components/checkbox.vue')
const LinkPage = () => import('~/pages/components/link.vue')
const PaginationPage = () => import('~/pages/components/pagination.vue')
const SpinnerPage = () => import('~/pages/components/spinner.vue')
const SkeletonPage = () => import('~/pages/components/skeleton.vue')

const routes: Array<RouteRecordRaw> = [
  // Component preview routes
  { path: '/', redirect: '/components/typography' },
  { path: '/components', redirect: '/components/typography' },
  {
    path: '/components/typography',
    name: 'Typography',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: TypographyPage,
  },
  {
    path: '/components/button',
    name: 'Button',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: ButtonPage,
  },
  {
    path: '/components/alert',
    name: 'Alert',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: AlertPage,
  },
  {
    path: '/components/cards',
    name: 'Cards',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: CardsPage,
  },
  {
    path: '/components/input',
    name: 'Input',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: InputPage,
  },
  {
    path: '/components/empty',
    name: 'Empty',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: EmptyPage,
  },
  {
    path: '/components/modal',
    name: 'Modal',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: ModalPage,
  },
  {
    path: '/components/switch',
    name: 'Switch',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: SwitchPage,
  },
  {
    path: '/components/radio',
    name: 'Radio',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: RadioPage,
  },
  {
    path: '/components/checkbox',
    name: 'Checkbox',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: CheckboxPage,
  },
  {
    path: '/components/link',
    name: 'Link',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: LinkPage,
  },
  {
    path: '/components/pagination',
    name: 'Pagination',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: PaginationPage,
  },
  {
    path: '/components/spinner',
    name: 'Spinner',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: SpinnerPage,
  },
  {
    path: '/components/skeleton',
    name: 'Skeleton',
    meta: {
      requiresAuth: false,
      layout: 'components',
    },
    component: SkeletonPage,
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        el: to.hash,
        behavior: 'smooth',
      }
    } else if (savedPosition) {
      return savedPosition
    } else {
      return {
        left: 0,
        top: 0,
      }
    }
  },
})

export default router
