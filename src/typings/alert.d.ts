// Alert
export type AlertVariant = 'secondary' | 'blue' | 'white' | 'error' | 'success'
