// Button
export type ButtonVariant =
  | 'default'
  | 'primary'
  | 'secondary'
  | 'blue'
  | 'outline'
  | 'outline-blue'
  | 'outline-secondary'
  | 'outline'
  | 'outline-grey-stroke'

export type ButtonShape = 'rounded' | 'circle' | 'square'
