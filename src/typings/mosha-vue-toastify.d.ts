import type { createToast as createToastType } from 'mosha-vue-toastify'

declare module 'mosha-vue-toastify' {
  const createToast: typeof createToastType
}
