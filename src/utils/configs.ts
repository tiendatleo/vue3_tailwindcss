export const GLOBAL_COMPONENT_PREFIX = 'v-'
export const DEFAULT_LAYOUT = 'default'

export const API_CODE_SUCCESS = 1
export const API_CODE_404 = 404
export const API_CODE_403 = 403
export const MIN_TIME_TRACKING_VIDEO = 5

export enum TYPE_ITEM {
  RECIPE = 'RECIPE',
  STORIES = 'STORIES',
  TIP_AND_TRICK = 'TIP_AND_TRICK',
  TECHNIQUES = 'TECHNIQUES',
  CHEF = 'CHEF',
}

export enum TYPE_USER {
  EMAIL = 1,
  APPLE = 2,
  GMAIL = 3,
  FACEBOOK = 4,
}
