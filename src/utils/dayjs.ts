import dayjs from 'dayjs'

export const DATE_TIME_FORMAT = 'DD/MM/YYYY HH:mm:ss'
export const DO_OF_FORMAT = 'Do [of] MMMM YYYY'

/**
 * Counter days difference between two dates in dayjs Js
 */
export function counterDays(endDate: Date, startDate = dayjs(new Date())) {
  return dayjs(endDate, DATE_TIME_FORMAT).diff(startDate, 'day')
}

/**
 * To update format date time Do [of] MMMM YYYY
 */
export function formatDateTime(
  currentDate: Date,
  formatType = DO_OF_FORMAT as string
) {
  return dayjs(currentDate, DATE_TIME_FORMAT).format(formatType)
}
