export function printWarning(message: string) {
  console.warn('[Vue-components] warning: ' + '\n' + message)
}
