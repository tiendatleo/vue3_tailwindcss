import { ToastOptions } from 'mosha-vue-toastify'

export const toastDefaultConfig = {
  showIcon: true,
  timeout: 3000,
}

export const toastSuccessConfig = {
  ...toastDefaultConfig,
  type: 'success',
  toastBackgroundColor: '#176335',
} as ToastOptions

export const toastErrorConfig = {
  ...toastDefaultConfig,
  type: 'danger',
  toastBackgroundColor: '#982313',
} as ToastOptions
