import { onMounted, onUnmounted } from 'vue'
import { onBeforeRouteLeave } from 'vue-router'

const showPrompt = (e: BeforeUnloadEvent) => {
  // Cancel the event
  e.preventDefault() // If you prevent default behavior in Mozilla Firefox prompt will always be shown
  // Chrome requires returnValue to be set
  e.returnValue = ''
}

const showConfirm = (message?: string) => {
  if (window.confirm(message || 'Do you really want to leave?')) {
    return true
  } else {
    return false
  }
}

const usePrompt = (message?: string, checkShouldPrompt?: () => boolean) => {
  const hanleBeforeUnload = (e: BeforeUnloadEvent) => {
    if (checkShouldPrompt) {
      if (checkShouldPrompt()) {
        showPrompt(e)
      }
    } else {
      showPrompt(e)
    }
  }

  onBeforeRouteLeave(() => {
    if (checkShouldPrompt) {
      if (checkShouldPrompt()) {
        return showConfirm(message)
      }
    } else {
      showConfirm(message)
    }
  })

  onMounted(() => {
    window.addEventListener('beforeunload', hanleBeforeUnload)
  })

  onUnmounted(() => {
    window.removeEventListener('beforeunload', hanleBeforeUnload)
  })
}

export default usePrompt
