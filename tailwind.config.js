// eslint-disable-next-line @typescript-eslint/no-var-requires
const defaultTheme = require('tailwindcss/defaultTheme')
const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: '#000',
      white: '#fff',
      lemon: '#FEC401',
      carrot: '#F06F5D',
      berry: '#DD98AF',
      sea: '#201F58',
      'forest-70': '#405B5E',
      basil: '#78AF8E',
      aqua: '#81D2EA',
      eggshell: '#FDFBF7',
      error: '#982313',
      success: '#176335',
      focus: '#007AFF',
      'sea-active': '#28267C',
      'peach-active': '#F29C63',
      'grey-stroke': '#CED4D0',
      'light-stroke': '#F0EDDC',
      primary: '#103236', // Forest
      secondary: '#FFB788', // Peach
      neutral: '#A35003', // Earth
      loading: '#EEEBD8',
    },
    container: {
      ...defaultTheme.container,
      center: true,
      padding: '1.25rem',
      screens: {
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px',
        '2xl': false,
      },
    },
    fontFamily: {
      sans: ['Effra', 'sans-serif'],
      serif: ['Gazpacho', 'serif'],
    },
    extend: {
      fontSize: {
        '17px': '1.0625rem',
      },
      lineHeight: {
        1.15: 1.15,
        1.1: 1.1,
      },
      letterSpacing: {
        0.2: '0.2em',
      },
    },
  },
  variants: {
    lineClamp: ['responsive'],
    aspectRatio: [],
    extend: {
      margin: ['first'],
      textColor: ['active'],
      borderColor: ['active'],
    },
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
    plugin(function({ addComponents, theme }) {
      const icon = {
        '.icon': {
          fill: theme('fill.current'),
          flexShrink: theme('flexShrink.0'),
          width: '1em',
          height: '1em',
        },
        '.icon-stroke': {
          stroke: theme('stroke.current'),
          flexShrink: theme('flexShrink.0'),
          width: '1em',
          height: '1em',
        }
      }
      
      addComponents(icon)
    }),
  ],
  corePlugins: {
    float: false,
  },
}
